package de.bitbeschleuniger;

import java.util.List;

public class CodeBreaker {
	private CodeChecker codeChecker;

	public CodeBreaker(CodeChecker codeChecker) {
		this.codeChecker = codeChecker;
	}
	
	public void guessCode() {
		Result result = new Result(0, 0);
		
		while(result.getRightPlacedNumbers() < 4) {
			List<Integer> guessedCode = CodeGenerator.generateCode(codeChecker.getSize());
			
			result = codeChecker.guessCode(guessedCode);
		}
	}
}
