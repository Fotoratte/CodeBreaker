package de.bitbeschleuniger.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.bitbeschleuniger.CodeBreaker;
import de.bitbeschleuniger.CodeChecker;

public class CodeBreakerTest {

	@Test
	public void testOne() {
		for (int i = 0; i < 10; i++) {
			List<Integer> code = new ArrayList<Integer>();
			code.add(1);
			code.add(2);
			code.add(3);
			code.add(4);
			CodeChecker codeChecker = new CodeChecker(code);

			CodeBreaker codeBreaker = new CodeBreaker(codeChecker);

			codeBreaker.guessCode();

			System.out.println(codeChecker.getCountGuess());
		}
	}
}
