package de.bitbeschleuniger.test;

import java.util.List;

import org.junit.Test;

import de.bitbeschleuniger.CodeGenerator;

public class CodeGeneratorTest {
	
	@Test
	public void testGenerate() {
		List<Integer> code = CodeGenerator.generateCode(4);
		
		for (Integer integer : code) {
			System.out.print(integer);
		}
	}

}
