package de.bitbeschleuniger.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import de.bitbeschleuniger.CodeChecker;
import de.bitbeschleuniger.Result;

public class CodeCheckerTest {
	
	@Test
	public void testOne() {
		List<Integer> code = new ArrayList<Integer>();
		code.add(1);
		code.add(2);
		code.add(3);
		code.add(4);
		CodeChecker codeChecker = new CodeChecker(code);
		
		Result result = codeChecker.guessCode(code);
		
		Assert.assertEquals(result.getRightNumbers(),4);
		Assert.assertEquals(result.getRightPlacedNumbers(),4);
	}
	
	@Test
	public void testTwo() {
		List<Integer> code = new ArrayList<Integer>();
		code.add(1);
		code.add(2);
		code.add(3);
		code.add(4);
		CodeChecker codeChecker = new CodeChecker(code);
		
		List<Integer> guestedCode = new ArrayList<Integer>();
		guestedCode.add(4);
		guestedCode.add(3);
		guestedCode.add(2);
		guestedCode.add(1);
		
		Result result = codeChecker.guessCode(guestedCode);
		
		Assert.assertEquals(result.getRightNumbers(),4);
		Assert.assertEquals(result.getRightPlacedNumbers(),0);
	}

}
