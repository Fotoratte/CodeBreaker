package de.bitbeschleuniger;

import java.util.List;

public class CodeChecker {
	private List<Integer> code;
	private int countGuess;

	public CodeChecker(List<Integer> code) {
		this.code = code;
		countGuess = 0;
	}

	public Result guessCode(List<Integer> guessedCode) {
		int rightNumbers = 0;
		int rightPlacedNumbers = 0;

		for (int i = 0; i < guessedCode.size(); i++) {
			Integer number = guessedCode.get(i);
			
			if(code.contains(number)) {
				rightNumbers++;
			}
			
			if(code.get(i) == number) {
				rightPlacedNumbers++;
			}
		}
		
		countGuess++;
		
		return new Result(rightNumbers, rightPlacedNumbers);
	}
	
	public int getSize() {
		return code.size();
	}

	public int getCountGuess() {
		return countGuess;
	}

}
