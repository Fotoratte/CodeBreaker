package de.bitbeschleuniger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CodeGenerator {

	static public List<Integer> generateCode(int n) {
		List<Integer> code = new ArrayList<Integer>();
		List<Integer> numbers = new ArrayList<Integer>();

		for (int i = 1; i < 10; i++) {
			numbers.add(i);
		}

		for (int i = 0; i < n; i++) {
			Random random = new Random();

			Integer number = numbers.remove(random.nextInt(numbers.size()));

			code.add(number);
		}

		return code;
	}
}
