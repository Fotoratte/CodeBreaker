package de.bitbeschleuniger;

public class Result {
	private int rightNumbers;
	private int rightPlacedNumbers;

	public Result(int rightNumbers, int rightPlacedNumbers) {
		super();
		this.rightNumbers = rightNumbers;
		this.rightPlacedNumbers = rightPlacedNumbers;
	}

	public int getRightNumbers() {
		return rightNumbers;
	}

	public void setRightNumbers(int rightNumbers) {
		this.rightNumbers = rightNumbers;
	}

	public int getRightPlacedNumbers() {
		return rightPlacedNumbers;
	}

	public void setRightPlacedNumbers(int rightPlacedNumbers) {
		this.rightPlacedNumbers = rightPlacedNumbers;
	}

}
